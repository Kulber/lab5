# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary: Tracks the changes made to the clock.cpp program 
* Version: 1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up: Uses strftime function to display the day, time, and year
* Configuration: C++, normal functions, a struct and all of this is called in strftime
* How to run tests: compile with g++ clock.cpp -o clock and then use ./clock

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact